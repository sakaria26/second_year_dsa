import 'service.pb as PB;

public table<PB:Author> key(author_id) authors = table[
    {author_id: 1, firstname: "George", lastname: "Martin"},
    {author_id: 2, firstname: "John", lastname: "Doe"},
    {author_id: 3, firstname: "Jane", lastname: "Doe"},
    {author_id: 4, firstname: "Patrick", lastname: "Bacster"}
];

public table<PB:Location> key(location_id) locations = table[
    {location_id: 1, floor: 1, section: "Fantasy", shelf_number: "A002", row: 2},
    {location_id: 2, floor: 2, section: "Geography", shelf_number: "A001", row: 4},
    {location_id: 3, floor: 3, section: "Computer Science", shelf_number: "A003", row: 5}
];
 
public table<PB:Book> key(isbn) books = table[
    {isbn: "9000-09-8934",title: "Introduction to Distributed System", authors: [{author_id: 2}], book_location: {location_id: 3}, status: true},
    {isbn: "9030-19-8204",title: "Fire and Blood", authors: [{author_id: 1}], book_location: {location_id: 1}, status: false},
    {isbn: "8940-78-8934",title: "Maps For Geologist", authors: [{author_id: 4}, {author_id: 3}], book_location: {location_id: 2}, status: true}
];

public table<PB:User> key(user_number) users = table[
    {user_number: 12, firstname: "Peter", lastname: "Doe", role: "student"}
];