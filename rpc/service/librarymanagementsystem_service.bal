import ballerina/grpc;
import 'service.pb as PB;
import 'service.datastore as Datastore;

listener grpc:Listener ep = new (9090);

@grpc:Descriptor {value: PB:LIBRARY_MANAGEMENT_SYSTEM_DESC}
service "LibraryManagementSystem" on ep {

    remote function add_book(PB:BookRequest value) returns PB:CreateBookResponse|error {
        Datastore:books.add(value.bookRequest);
        PB:CreateBookResponse response = {isbn: value.bookRequest.isbn};
        return response;
    }
    remote function update_book(PB:BookRequest value) returns PB:BookResponse|error {
        Datastore:books.put(value.bookRequest);
        PB:BookResponse response = {book: Datastore:books.get(value.bookRequest.isbn)};
        return response;
    }
    remote function locate_book(PB:LocateBookRequest value) returns PB:BookResponse|error {
        PB:Book book = Datastore:books.get(value.ISBN);
        PB:BookResponse response = {book: book};
        return response;
    }
    remote function borrow_book(PB:BorrowBookRequest value) returns string|error {
        PB:Book book = Datastore:books.get(value.ISBN);
        
        if book.status == true {
            book.status = false;
            Datastore:books.put(book);
            return "Book successfully borrowed";
        }

        return error("That book is unavailable");
        
    }
    remote function return_book(PB:BookRequest value) returns string|error {
        value.bookRequest.status = true;
        Datastore:books.put(value.bookRequest);
        return "Thank you for returning the book";
    }
    remote function create_users(stream<PB:UserRequest, grpc:Error?> clientStream) returns string|error {
        _ = check clientStream.forEach(function (PB:UserRequest userRequest) {
            foreach PB:User item in userRequest.users {
                Datastore:users.add(item);
            }
        });
        return string `Users created successfully`;
    }
    remote function remove_book(PB:RemoveBookRequest value) returns stream<PB:BookResponse, error?>|error {
        _ = Datastore:books.remove(value.ISBN);
        PB:Book[] remaining_books = Datastore:books.toArray();
        PB:BookResponse[] response = [];
        foreach PB:Book item in remaining_books {
            PB:BookResponse book = {book:item};
            response.push(book);
        }
        
        return response.toStream();
    }
    remote function list_available_books(PB:ListAvailableRequest value) returns stream<PB:BookResponse, error?>|error {
        PB:BookResponse[] response = [];
        foreach PB:Book item in Datastore:books.toArray() {
            if item.status == true{
                PB:BookResponse book = {book:item};
                response.push(book);
            }
        }

        return response.toStream();
    }
}

