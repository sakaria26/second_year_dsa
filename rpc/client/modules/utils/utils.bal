import 'client.pb as PB;
import ballerina/grpc;
import ballerina/io;

PB:LibraryManagementSystemClient ep = check new ("http://localhost:9090");
# unary
public function add_book() {
    string isbn = "8850-38-4934";
    string title = "A Dance with Dragons";
    boolean status = true;

    PB:Author[] author = [{author_id: 1, firstname: "George", lastname: "Martin"}];
    PB:Book book = {isbn:isbn, title:title, authors: author, book_location: {location_id: 0}, status: status};

    PB:BookRequest bookRequest = {bookRequest: book};
    do {
        PB:CreateBookResponse|error result =  ep->add_book(bookRequest);
        io:print(result);
       
    } on fail error e {
        io:println(`Error: ${e}`);
    }
}

# client
public function create_users() {
    do {
        PB:Create_usersStreamingClient|grpc:Error createUsersStreamingClient = check ep->create_users();
        grpc:Error? error1;
        grpc:Error? error2;

        PB:User[] users = [
            {
                user_number: 34,
                firstname: "Jake",
                lastname: "Jackson",
                role: "student"
            },
            {
                user_number: 35,
                firstname: "Janet",
                lastname: "Jackson",
                role: "librarian"
            },
            {
                user_number: 36,
                firstname: "Lillie",
                lastname: "Gregson",
                role: "admin"
            }
        ];

        PB:UserRequest[] userRequest = [{users: users}];
        if createUsersStreamingClient is error {
            
        } else {
            foreach PB:UserRequest item in userRequest {
                error1 = createUsersStreamingClient->sendUserRequest(item);
            }
            error2 = createUsersStreamingClient->complete();

            string|grpc:Error? result = createUsersStreamingClient->receiveString();
            io:println(error1);
            io:println(error2);
            io:println(result);
        }
        
    } on fail var varName {
        io:println(varName);
    }


}

# unary
public function update_book() {
    PB:BookRequest request = {bookRequest: {isbn: "9000-09-8934", title:"Beyond Distributed Systems"}};

    do {
        PB:BookResponse|grpc:Error? response = check ep->update_book(request);
        io:println(response);

    }on fail var varName {
        io:println(varName);
    }
}

# server
public function remove_book() {
    PB:RemoveBookRequest request = {ISBN: "9000-09-8934"};
    do {
        stream<PB:BooksResponse, grpc:Error?> result = check ep->remove_book(request);
        check result.forEach(function(PB:BooksResponse response){
            io:println(response);
        });
    }on fail var varName {
        io:println(varName);
    }

}

# server
public function list_available_books() {
    PB:ListAvailableRequest request = {};
    do {
        stream<PB:BooksResponse, grpc:Error?> result = check ep->list_available_books(request);
        check result.forEach(function(PB:BooksResponse response){
            io:println(response);
        });
    } on fail var varName {
        io:println(varName);
    }
}

# unary
public function locate_book() {
    PB:LocateBookRequest request = {ISBN: "9000-09-8934"};
    do {
        PB:BookResponse|error result =  ep->locate_book(request);
        io:print(result);
       
    } on fail error e {
        io:println(`Error: ${e}`);
    }
}

# unary
public function borrow_book() {
    PB:BorrowBookRequest request = {ISBN: "9000-09-8934", user_number: 12};
    do {
        string|error result =  ep->borrow_book(request);
        io:print(result);
       
    } on fail error e {
        io:println(`Error: ${e}`);
    }
}

# unary
public function return_book() {
    PB:BookRequest request = {bookRequest: {isbn: "9000-09-8934"}};
    do {
        string|error result =  ep->return_book(request);
        io:print(result);
       
    } on fail error e {
        io:println(`Error: ${e}`);
    }
}