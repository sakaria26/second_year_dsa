import ballerina/grpc;
import ballerina/protobuf;
import ballerina/protobuf.types.wrappers;

public const string LIBRARY_MANAGEMENT_SYSTEM_DESC = "0A1F6C6962726172795F6D616E6167656D656E745F73797374656D2E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F225F0A06417574686F72121B0A09617574686F725F69641801200128055208617574686F724964121C0A0966697273746E616D65180220012809520966697273746E616D65121A0A086C6173746E616D6518032001280952086C6173746E616D652290010A084C6F636174696F6E121F0A0B6C6F636174696F6E5F6964180120012805520A6C6F636174696F6E496412140A05666C6F6F721802200128055205666C6F6F7212180A0773656374696F6E180320012809520773656374696F6E12210A0C7368656C665F6E756D626572180420012809520B7368656C664E756D62657212100A03726F771805200128055203726F77229B010A04426F6F6B12120A046973626E18012001280952046973626E12140A057469746C6518022001280952057469746C6512210A07617574686F727318032003280B32072E417574686F725207617574686F7273122E0A0D626F6F6B5F6C6F636174696F6E18042001280B32092E4C6F636174696F6E520C626F6F6B4C6F636174696F6E12160A06737461747573180520012808520673746174757322360A0B426F6F6B5265717565737412270A0B626F6F6B5265717565737418012001280B32052E426F6F6B520B626F6F6B5265717565737422280A12437265617465426F6F6B526573706F6E736512120A046973626E18012001280952046973626E22270A1152656D6F7665426F6F6B5265717565737412120A044953424E18012001280952044953424E222C0A0D426F6F6B73526573706F6E7365121B0A05626F6F6B7318012003280B32052E426F6F6B5205626F6F6B7322160A144C697374417661696C61626C655265717565737422270A114C6F63617465426F6F6B5265717565737412120A044953424E18012001280952044953424E22480A11426F72726F77426F6F6B5265717565737412120A044953424E18012001280952044953424E121F0A0B757365725F6E756D626572180220012805520A757365724E756D62657222750A0455736572121F0A0B757365725F6E756D626572180120012805520A757365724E756D626572121C0A0966697273746E616D65180220012809520966697273746E616D65121A0A086C6173746E616D6518032001280952086C6173746E616D6512120A04726F6C651804200128095204726F6C65222A0A0B5573657252657175657374121B0A05757365727318012003280B32052E557365725205757365727322290A0C426F6F6B526573706F6E736512190A04626F6F6B18012001280B32052E426F6F6B5204626F6F6B32D6030A174C6962726172794D616E6167656D656E7453797374656D122D0A086164645F626F6F6B120C2E426F6F6B526571756573741A132E437265617465426F6F6B526573706F6E7365123C0A0C6372656174655F7573657273120C2E55736572526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C75652801122A0A0B7570646174655F626F6F6B120C2E426F6F6B526571756573741A0D2E426F6F6B526573706F6E736512330A0B72656D6F76655F626F6F6B12122E52656D6F7665426F6F6B526571756573741A0E2E426F6F6B73526573706F6E73653001123F0A146C6973745F617661696C61626C655F626F6F6B7312152E4C697374417661696C61626C65526571756573741A0E2E426F6F6B73526573706F6E7365300112300A0B6C6F636174655F626F6F6B12122E4C6F63617465426F6F6B526571756573741A0D2E426F6F6B526573706F6E7365123F0A0B626F72726F775F626F6F6B12122E426F72726F77426F6F6B526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512390A0B72657475726E5F626F6F6B120C2E426F6F6B526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565620670726F746F33";

public isolated client class LibraryManagementSystemClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, LIBRARY_MANAGEMENT_SYSTEM_DESC);
    }

    isolated remote function add_book(BookRequest|ContextBookRequest req) returns CreateBookResponse|grpc:Error {
        map<string|string[]> headers = {};
        BookRequest message;
        if req is ContextBookRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("LibraryManagementSystem/add_book", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <CreateBookResponse>result;
    }

    isolated remote function add_bookContext(BookRequest|ContextBookRequest req) returns ContextCreateBookResponse|grpc:Error {
        map<string|string[]> headers = {};
        BookRequest message;
        if req is ContextBookRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("LibraryManagementSystem/add_book", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <CreateBookResponse>result, headers: respHeaders};
    }

    isolated remote function update_book(BookRequest|ContextBookRequest req) returns BookResponse|grpc:Error {
        map<string|string[]> headers = {};
        BookRequest message;
        if req is ContextBookRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("LibraryManagementSystem/update_book", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <BookResponse>result;
    }

    isolated remote function update_bookContext(BookRequest|ContextBookRequest req) returns ContextBookResponse|grpc:Error {
        map<string|string[]> headers = {};
        BookRequest message;
        if req is ContextBookRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("LibraryManagementSystem/update_book", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <BookResponse>result, headers: respHeaders};
    }

    isolated remote function locate_book(LocateBookRequest|ContextLocateBookRequest req) returns BookResponse|grpc:Error {
        map<string|string[]> headers = {};
        LocateBookRequest message;
        if req is ContextLocateBookRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("LibraryManagementSystem/locate_book", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <BookResponse>result;
    }

    isolated remote function locate_bookContext(LocateBookRequest|ContextLocateBookRequest req) returns ContextBookResponse|grpc:Error {
        map<string|string[]> headers = {};
        LocateBookRequest message;
        if req is ContextLocateBookRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("LibraryManagementSystem/locate_book", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <BookResponse>result, headers: respHeaders};
    }

    isolated remote function borrow_book(BorrowBookRequest|ContextBorrowBookRequest req) returns string|grpc:Error {
        map<string|string[]> headers = {};
        BorrowBookRequest message;
        if req is ContextBorrowBookRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("LibraryManagementSystem/borrow_book", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function borrow_bookContext(BorrowBookRequest|ContextBorrowBookRequest req) returns wrappers:ContextString|grpc:Error {
        map<string|string[]> headers = {};
        BorrowBookRequest message;
        if req is ContextBorrowBookRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("LibraryManagementSystem/borrow_book", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function return_book(BookRequest|ContextBookRequest req) returns string|grpc:Error {
        map<string|string[]> headers = {};
        BookRequest message;
        if req is ContextBookRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("LibraryManagementSystem/return_book", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function return_bookContext(BookRequest|ContextBookRequest req) returns wrappers:ContextString|grpc:Error {
        map<string|string[]> headers = {};
        BookRequest message;
        if req is ContextBookRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("LibraryManagementSystem/return_book", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function create_users() returns Create_usersStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("LibraryManagementSystem/create_users");
        return new Create_usersStreamingClient(sClient);
    }

    isolated remote function remove_book(RemoveBookRequest|ContextRemoveBookRequest req) returns stream<BooksResponse, grpc:Error?>|grpc:Error {
        map<string|string[]> headers = {};
        RemoveBookRequest message;
        if req is ContextRemoveBookRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("LibraryManagementSystem/remove_book", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        BooksResponseStream outputStream = new BooksResponseStream(result);
        return new stream<BooksResponse, grpc:Error?>(outputStream);
    }

    isolated remote function remove_bookContext(RemoveBookRequest|ContextRemoveBookRequest req) returns ContextBooksResponseStream|grpc:Error {
        map<string|string[]> headers = {};
        RemoveBookRequest message;
        if req is ContextRemoveBookRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("LibraryManagementSystem/remove_book", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        BooksResponseStream outputStream = new BooksResponseStream(result);
        return {content: new stream<BooksResponse, grpc:Error?>(outputStream), headers: respHeaders};
    }

    isolated remote function list_available_books(ListAvailableRequest|ContextListAvailableRequest req) returns stream<BooksResponse, grpc:Error?>|grpc:Error {
        map<string|string[]> headers = {};
        ListAvailableRequest message;
        if req is ContextListAvailableRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("LibraryManagementSystem/list_available_books", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        BooksResponseStream outputStream = new BooksResponseStream(result);
        return new stream<BooksResponse, grpc:Error?>(outputStream);
    }

    isolated remote function list_available_booksContext(ListAvailableRequest|ContextListAvailableRequest req) returns ContextBooksResponseStream|grpc:Error {
        map<string|string[]> headers = {};
        ListAvailableRequest message;
        if req is ContextListAvailableRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("LibraryManagementSystem/list_available_books", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        BooksResponseStream outputStream = new BooksResponseStream(result);
        return {content: new stream<BooksResponse, grpc:Error?>(outputStream), headers: respHeaders};
    }
}

public client class Create_usersStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendUserRequest(UserRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextUserRequest(ContextUserRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveString() returns string|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return payload.toString();
        }
    }

    isolated remote function receiveContextString() returns wrappers:ContextString|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: payload.toString(), headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public class BooksResponseStream {
    private stream<anydata, grpc:Error?> anydataStream;

    public isolated function init(stream<anydata, grpc:Error?> anydataStream) {
        self.anydataStream = anydataStream;
    }

    public isolated function next() returns record {|BooksResponse value;|}|grpc:Error? {
        var streamValue = self.anydataStream.next();
        if (streamValue is ()) {
            return streamValue;
        } else if (streamValue is grpc:Error) {
            return streamValue;
        } else {
            record {|BooksResponse value;|} nextRecord = {value: <BooksResponse>streamValue.value};
            return nextRecord;
        }
    }

    public isolated function close() returns grpc:Error? {
        return self.anydataStream.close();
    }
}

public client class LibraryManagementSystemCreateBookResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendCreateBookResponse(CreateBookResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextCreateBookResponse(ContextCreateBookResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class LibraryManagementSystemBooksResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendBooksResponse(BooksResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextBooksResponse(ContextBooksResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class LibraryManagementSystemStringCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendString(string response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextString(wrappers:ContextString response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class LibraryManagementSystemBookResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendBookResponse(BookResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextBookResponse(ContextBookResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextBooksResponseStream record {|
    stream<BooksResponse, error?> content;
    map<string|string[]> headers;
|};

public type ContextUserRequestStream record {|
    stream<UserRequest, error?> content;
    map<string|string[]> headers;
|};

public type ContextBookResponse record {|
    BookResponse content;
    map<string|string[]> headers;
|};

public type ContextLocateBookRequest record {|
    LocateBookRequest content;
    map<string|string[]> headers;
|};

public type ContextBooksResponse record {|
    BooksResponse content;
    map<string|string[]> headers;
|};

public type ContextListAvailableRequest record {|
    ListAvailableRequest content;
    map<string|string[]> headers;
|};

public type ContextRemoveBookRequest record {|
    RemoveBookRequest content;
    map<string|string[]> headers;
|};

public type ContextBorrowBookRequest record {|
    BorrowBookRequest content;
    map<string|string[]> headers;
|};

public type ContextBookRequest record {|
    BookRequest content;
    map<string|string[]> headers;
|};

public type ContextCreateBookResponse record {|
    CreateBookResponse content;
    map<string|string[]> headers;
|};

public type ContextUserRequest record {|
    UserRequest content;
    map<string|string[]> headers;
|};

@protobuf:Descriptor {value: LIBRARY_MANAGEMENT_SYSTEM_DESC}
public type User record {|
    readonly int user_number;
    string firstname?;
    string lastname?;
    string role?;
|};

@protobuf:Descriptor {value: LIBRARY_MANAGEMENT_SYSTEM_DESC}
public type RemoveBookRequest record {|
    string ISBN;
|};

@protobuf:Descriptor {value: LIBRARY_MANAGEMENT_SYSTEM_DESC}
public type CreateBookResponse record {|
    string isbn;
|};

@protobuf:Descriptor {value: LIBRARY_MANAGEMENT_SYSTEM_DESC}
public type BookResponse record {|
    Book book;
|};

@protobuf:Descriptor {value: LIBRARY_MANAGEMENT_SYSTEM_DESC}
public type LocateBookRequest record {|
    string ISBN;
|};

@protobuf:Descriptor {value: LIBRARY_MANAGEMENT_SYSTEM_DESC}
public type BooksResponse record {|
    Book[] books;
|};

@protobuf:Descriptor {value: LIBRARY_MANAGEMENT_SYSTEM_DESC}
public type ListAvailableRequest record {|
|};

@protobuf:Descriptor {value: LIBRARY_MANAGEMENT_SYSTEM_DESC}
public type Book record {|
    readonly string isbn;
    string title?;
    Author[] authors?;
    Location book_location?;
    boolean status?;
|};

@protobuf:Descriptor {value: LIBRARY_MANAGEMENT_SYSTEM_DESC}
public type BorrowBookRequest record {|
    string ISBN;
    int user_number;
|};

@protobuf:Descriptor {value: LIBRARY_MANAGEMENT_SYSTEM_DESC}
public type Author record {|
    readonly int author_id;
    string firstname?;
    string lastname?;
|};

@protobuf:Descriptor {value: LIBRARY_MANAGEMENT_SYSTEM_DESC}
public type BookRequest record {|
    Book bookRequest;
|};

@protobuf:Descriptor {value: LIBRARY_MANAGEMENT_SYSTEM_DESC}
public type UserRequest record {|
    User[] users;
|};

@protobuf:Descriptor {value: LIBRARY_MANAGEMENT_SYSTEM_DESC}
public type Location record {|
    readonly int location_id;
    int floor?;
    string section?;
    string shelf_number?;
    int row?;
|};

